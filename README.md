# aginicx-code-challenge
The code challenge for internship application at AginicX.

### Description

There are two main components to this challenge, a REST server and front end.

1. Rest server

	- /geocode (POST)
	Allows to submit a JSON array containing names and addresses of locations. The server should then return an ID that maps to a running job. The running job should go through the addresses and use Google's geocoding API to map each name to a latitude and longitude.

	- /geocode/:id (GET)
	The second endpoint can be given the previously mentioned ID to get either a message saying the job is still pending, or the resulting array of coordinates.


2. Front end

	Create a frontend which consumes this API by allowing user to select a csv file consisting of name and address pairs, and then showing the pending status of the job, and the final dataset.

### Simple user journey:

1. User selects a CSV file consisting of a number of name and address pairs
2. User uploads CSV file
3. User see's confirmation of valid (or show err for invalid) CSV and that conversion has started running
4. While on page, user can see completion status of running job (running, complete, expired)
5. When conversion is complete, return data to this page in user friendly formats

### UX considerations:
    
- Users will only be able to access their pending or completed data if they have the ID of the running job.

	- *Solution*: returned ID should be visually emphasised and easily copied.

- Returned data is not perpetually accessible - assumed storage restrictions means data expires every 3 hours.  

	- *Solution*: make the 3 hour expiration time clear to user.


