'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    maps = require('gulp-sourcemaps');


// dependant tasks

// gulp.task('concatScripts', function () {
//     return gulp.src([
//         './src/public/js/vendor/modernizr-2.8.3.min.js',
//         './src/public/js/main.js'])
//         .pipe(maps.init())
//         .pipe(concat('app.js'))
//         .pipe(maps.write('./'))
//         .pipe(gulp.dest('./src/public/js/'));
// });

// gulp.task('minifyScripts', ['concatScripts'], function () {
//     return gulp.src('./src/public/js/app.js')
//         .pipe(uglify())
//         .pipe(rename('app.min.js'))
//         .pipe(gulp.dest('./src/public/js/'));
// });

gulp.task('compileSass', function () {
    return gulp.src('./src/scss/index.scss')
        .pipe(maps.init())
        .pipe(sass({
            includePaths: require('node-normalize-scss').includePaths
        }).on('error', sass.logError))
        .pipe(maps.write('./'))
        .pipe(gulp.dest('./src/public/css/'))
});


// development tasks

gulp.task('watch', function () {
    gulp.watch('src/scss/**/*.scss', ['compileSass']);
    // gulp.watch('src/public/js/main.js', ['concatScripts']);
});


// pipes

gulp.task('init', function () {
    gulp.start('watch');
});





























