const rp = require('request-promise-native');
const kue = require('kue');


let redisConfig;
if (process.env.NODE_ENV === 'production') {
    redisConfig = {
        redis: {
            port: process.env.REDIS_PORT,
            host: process.env.REDIS_HOST,
            auth: process.env.REDIS_PASS
        }
    };
} else {
    redisConfig = {};
}

const queue = kue.createQueue(redisConfig);

// geocode an address using google geocoding API - returns promise
async function geocodeAddress(inputAddress) {
    let geocoded;
    const key = process.env.GEOCODING_API_KEY;
    const address = inputAddress.address;
    const inputURI = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${key}`;
    try {
        geocoded = await rp({
            method: 'POST',
            uri: inputURI,
            json: true, // Automatically parses the JSON string in the response
        });
    } catch (err) {
        // error getting location
        console.log(err);
        return (err);
    };
    // return with only name & location data
    return {
        name: inputAddress.name,
        location: geocoded.results[0].geometry.location
    };
}

// process of geocode jobs
queue.process('geocode', 4, (job, done) => {
    // build array of promises from addresses - each resolves location obj from geocoding API
    const addresses = job.data.addresses;
    const promises = addresses.map(addressObj => geocodeAddress(addressObj));
    // once all promises resolve, return data & complete process
    return Promise.all(promises)
        .then((data) => {
            job.data.output = data;
            job.update();
            done();

            // set delay for testing to 6 seconds
            // setTimeout(() => { 
            //     job.data.output = data;
            //     job.update();
            //     done();
            // }, 6000);
        })
        .catch(function (err) {
            console.log(err);
            return done(err);
        });
});

const removeCompleted = () => {
    kue.Job.rangeByState('complete', 0, 5, 'asc', function (err, jobs) {
        jobs.forEach(function (job) {
            const completedAt = job.updated_at;
            const now = Math.floor(new Date());
            const hoursSince = (now - completedAt) / 3600000;
            if (hoursSince > 3) {
                job.remove();
            }
        });
    });
}

// create a new geocode job
const newGeocodeJob = (data, callback) => {
    let job = queue.create('geocode', {
            title: 'get location data',
            addresses: data.addresses
        })
        .attempts(8)
        .backoff(true)
        .removeOnComplete(false)
        .save(err => {
            if (err) return callback(err);
            if (!err) {
                // console.log(`Job ${job.id} saved to the queue.`);
                callback(null, job.id);
            }
        });
    // every time a job is added, check to remove expired job
    removeCompleted();
}

// find existing geocode job
const findGeocodeJob = (jobID, callback) => {
    // search for job by ID
    kue.Job.get(jobID, function (err, job) {
        // check status and throw error if no longer active
        if (err) {
            // return error - no token found
            const err = new Error("This token is invalid or has expired. Please check your token and try again. Remember all geocoding jobs expire after three hours.");
            err.status = 404;
            return callback(err)
        } else { // otherwise return job
            // console.log(`Job ${job.id} has been found successfully.`);
            callback(null, job);
        }
    });
}

module.exports.newGeocodeJob = (data, callback) => {
    newGeocodeJob(data, callback);
};

module.exports.findGeocodeJob = (jobID, callback) => {
    findGeocodeJob(jobID, callback);
};


