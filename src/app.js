const cluster = require('cluster');

if (cluster.isMaster) {

    const numWorkers = require('os').cpus().length;

    console.log('Master cluster setting up ' + numWorkers + ' workers...');

    for (let i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function (worker) {
        console.log('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function (worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        console.log('Starting a new worker');
        cluster.fork();
    });

} else {

    const express = require('express');
    const app = express();
    const router = express.Router();
    const logger = require('morgan');
    const bodyParser = require('body-parser');

    const primaryRoutes = require('./routes/primary_routes.js');
    const apiRoutes = require('./routes/api_routes.js');

    // .env safe
    app.enable('trust proxy');
    require('dotenv-safe').load();

    // errors 
    // app.use(logger("dev"));
    // app.use(flash());

    // parse JSON middleware
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    // set pug view engine
    app.set('view engine', 'pug');
    app.set('views', __dirname + '/templates');
    // set custom path for static content
    app.use('/static', express.static(__dirname + '/public'));

    // set route handlers 
    app.use('/', primaryRoutes);
    app.use('/geocode', apiRoutes);

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        const err = new Error("Not Found");
        err.status = 404;
        next(err);
    });

    // error handler
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            error: {
                message: err.message
            }
        });
    });

    // listen ----
    const port = process.env.PORT || 3000;

    app.listen(port, function () {
        console.log('Express server is running on port ' + port);
    });

}







