'use-strict';

const express = require('express');
const router = express.Router();
const geocode = require("../queue/geocode.js");

// REST API routes ---- 

// POST /geocode 
// create new geocoding conversion job

router.post('/', function (req, res, next) {
    // validate req body
    if (!req.body.csvData || !req.body.csvData[0].name ) {
        const err = new Error("Bad request. Please ensure your addresses and .csv formatting are valid.");
        err.status = 400;
        return next(err);
    } else {
        const reqAddesses = req.body.csvData;
        // create new running job (queue/geocode.js) - returns jobID
        geocode.newGeocodeJob({ addresses: reqAddesses }, (err, jobID) => {
            if (err) {
                console.error(err);
                return next(err);
            }
            const runningJobID = jobID;
            res.json({ "id": runningJobID });
        });
    }
});

// GET /geocode/:id
// get existing geocode conversion job

router.get('/:id', function (req, res, next) {
    const jobID = req.params.id;
    // find running job (queue/geocode.js) - returns job
    geocode.findGeocodeJob(jobID, (err, job) => {
        if (err) {
            console.error(err);
            return next(err);
        }
        const runningJob = job;
        res.json({ "job": runningJob });
    });

});

module.exports = router;