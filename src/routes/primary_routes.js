'use-strict';

const express = require('express');
const router = express.Router();

// static routes ---- 

// serve frontend here
router.get('/', function (req, res, next) {

    res.render('home', {
        pageClass: 'home-page',
        stage: 'upload'
    });

});

module.exports = router;