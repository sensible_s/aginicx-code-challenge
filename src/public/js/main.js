

// declare UI vars to reduce dom traversal

// general UI

const $allSections = $('section'),
      $inputSection = $('section.input'),
      $statusSection = $('section.status'),
      $resultsSection = $('section.results'),
      $allCrumbs = $('.breadcrumbs a'),
      $inputCrumb = $('[data_section="input"]'),
      $statusCrumb = $('[data_section="status"]'),
      $resultsCrumb = $('[data_section="results"]'),
      $searchForm = $("form.search"),
      $loader = $(".loader-container");

// input UI

const $pasteBtn = $('#pasteCSV button'),
    $uploadBtn = $('.upload-button'),
    $pasteCSV = $('#pasteCSV'),
    $fileInput = $("#fileInput");

// status UI
const $statusBtn = $("a.check-status"),
      $statusCircle = $('.status .circle'),
      $statusDesc = $('.status p.description'),
      $token = $('h3.token');

// output UI
const $outputType = $(".output form"),
      $previews = $('.preview pre'),
      $jsonPreview = $('.preview pre.json'),
      $csvPreview = $('.preview pre.csv'),
      $downloadBtn = $('.output .download');



// UI functions ----

function switchActive(section) {
    $allCrumbs.removeClass('active');
    $allSections.removeClass('active');
    switch (section) {
        case 'input':
            $inputSection.addClass('active');
            $inputCrumb.addClass('active');
            break;
        case 'status':
            $statusSection.addClass('active');
            $statusCrumb.addClass('active');
            break;
        case 'results':
            $resultsSection.addClass('active');
            $resultsCrumb.addClass('active');
            break;
        case 'error':
            $('section.error').addClass('active');
            break;
    }
}

function statusCircle(newClass, text) {
    // remove last state class
    var lastClass = $statusCircle.attr('class').split(' ').pop();
    $statusCircle.removeClass(lastClass)
    // add new status
    $statusCircle.addClass(newClass);
    // update text
    $statusDesc.text(text);
}

function previewType(type) {
    $previews.removeClass('active');
    switch (type) {
        case 'csv':
            $csvPreview.addClass('active');
            break;
        case 'json':
            $jsonPreview.addClass('active');
            break;
    }
}


// data editing functions ----

// convert csv input data to JSON
function csvJSON(csv) {
    let result = [];
    const lines = csv.split("\n");
    const inputHeaders = lines[0].split(",");
    // ignore wording of input headers and set these to reduce API error
    const headers = ["name", "address"];
    // ensure user is only uploading name and address pairs
    if (inputHeaders.length != 2) {
        const headersText = inputHeaders.join(', ');
        return {
            err: `Please ensure your csv file consists of only name and address pairs. Currently your .csv headers are: ${headersText}`
        }
    }
    // start at line 2  to skip headers
    for (let i = 1; i < lines.length; i++) {
        let obj = {};
        // remove escaped &: single & double quotes
        let currentLine = lines[i].replace(/[\\"']+/g, '');
        // use regex to split line only after first comma
        currentLine = currentLine.split(/,(.+)/);
        for (let i = 0; i < headers.length; i++) {
            obj[headers[i]] = currentLine[i];
        }
        result.push(obj);
    }
    return result;
}

// convert JSON result back to csv
function jsonCSV(json) {
    let result = 'name, latitude, longitude \n';
    json.forEach(place => {
        let line = `${place.name}, ${place.location.lat}, ${place.location.lng} \n`;
        result += line;
    });
    return result;
}

function downloadFile(data, format) {
    // create file and start download
    const exportFilename = `geocoded.${format}`;
    const blob = new Blob([data], { type: `text/${format};charset=utf-8;` });
    const link = document.createElement("a");
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", exportFilename);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

const onUpload = () => {
    const fileList = $fileInput.get(0).files;
    const file = fileList[0];

    // ensure a file is selected
    if (fileList.length === 0) {
        return { err: "Please select a file" };
    }
    // validate is .csv extension
    if (file.type !== 'text/csv') {
        return { err: "Please upload a valid .csv file" };
    }

    // read .csv file
    const fileReader = new FileReader();
    fileReader.onload = e => {
        const json = csvJSON(fileReader.result);
        processCsv(json);
    };
    fileReader.onerror = function (e) {
        throw 'Error reading CSV file';
    };

    // Start reading file
    fileReader.readAsText(file);
}

const processCsv = csvData => {
    if (csvData.err) {
        const $uploadFeedback = $('p.file-feedback');
        $uploadFeedback.text(csvData.err);
        $uploadFeedback.show();
    } else {
        postNewJob(csvData);
    }
}



// ajax functions ----

function postNewJob(csvData) {
    $loader.toggleClass('disabled');
    $.ajax({
        type: 'POST',
        url: '/geocode',
        contenType: 'application/json',
        data: { csvData: csvData },
        success: function (data) {
            setTimeout(() => {
                $loader.toggleClass('disabled');
            }, 500);
            // breadcrumb feedback
            $inputCrumb.addClass('complete');
            $statusCrumb.addClass('current');
            // update token text 
            $token.text(data.id);
            // switch to status section
            switchActive('status');
        },
        error: (data) => {
            setTimeout(() => {
                $loader.toggleClass('disabled');
            }, 500);
            $('section.error h2').text(data.statusText);
            $('section.error p').text(data.responseJSON.error.message);
            switchActive('error');
        }
    });
}

function updateStatus(jobNo, updateUI) {
    $loader.toggleClass('disabled');
    $.ajax({
        type: 'GET',
        url: '/geocode/' + jobNo,
        success: data => {
            // ensure visual feedback is given to user
            setTimeout(() => {
                $loader.toggleClass('disabled');
            }, 500);
            // update UI if this status update req came from search bar
            if (updateUI) {
                $token.text(jobNo);
            }

            switch (data.job.state) {
                case 'active':
                    switchActive('status');
                    statusCircle('running', 'Converting your addresses');
                    break;
                case 'complete':
                    $allCrumbs.addClass('complete');
                    $allCrumbs.removeClass('error');
                    statusCircle('complete', 'Geocoding complete');
                    // update preview data
                    const outputData = data.job.data.output;
                    $jsonPreview.text(JSON.stringify(outputData, null, 3));
                    $csvPreview.text(jsonCSV(outputData).toString());
                    // switch to results section
                    switchActive('results');
                    break;
                case 'failed':
                    $statusCrumb.addClass('error');
                    $resultsCrumb.addClass('error');
                    statusCircle('error', 'Geocoding failed');
                    // show error message
                    $('section.results .output').hide();
                    $('.error-container').show();
                    // switch to results section
                    switchActive('results');
                    break;
            }
        },
        error: data => {
            setTimeout(() => {
                $loader.toggleClass('disabled');
            }, 500);
            $('section.error h2').text(data.statusText);
            $('section.error p').text(data.responseJSON.error.message);
            switchActive('error');
        }
    });
}




// UI EVENTS

// general ui ---- 

$allCrumbs.click(e => {
    const $breadcrumb = $(e.currentTarget);
    // force users to complete steps in order
    if ($breadcrumb.hasClass('')) {
        return;
    } else {
        // switch to appropriate section
        switchActive($breadcrumb.attr('data_section'));
    }
});
$searchForm.submit(e => {
    e.preventDefault();
    const token = $('#search').val();
    updateStatus(token, true);
});


// input section ----

$fileInput.change(e => {
    const file = $fileInput.get(0).files[0];
    // reduce visual emph of file selection button
    $('label[for="fileInput"]').addClass('post-selection');
    // update filename text
    $('.file-preview p').text(file.name);
    // show file preview
    $('.file-preview').show();
});
$pasteBtn.click(e => {
    e.preventDefault();
    const csvText = $('#pasteCSV textarea').val();
    const csvData = csvJSON(csvText);
    processCsv(csvData);
});
$uploadBtn.click(e => {
    e.preventDefault();
    onUpload();
});


// status section ----

$statusBtn.click(() => {
    const token = $('.status h3.token').text();
    updateStatus(token, false);
});


$('a.copy-token').click(e => {
    const range = document.createRange();
    const selection = window.getSelection();
    range.selectNodeContents(document.querySelector('h3.token'));
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
});


// results section ----

$outputType.change(() => {
    previewType( $('input[name="outputType"]:checked').val() );
});

$downloadBtn.click(() => {
    const data = $('.output pre.active').text();
    const type = $('input[name="outputType"]:checked').val();
    downloadFile(data, type);
});
